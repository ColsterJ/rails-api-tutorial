Diffs
=====

**Basic Routing:**

- [`002-003:` Simple GET Endpoint With Routing](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/003-simple-get-endpoint-with-routing%0D002-create-rails-api-project-instead#diff)
- [`003-004:` Simple GET Endpoint With Routing (Minor Improvements)](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/004-simple-get-endpoint-with-routing-improvements%0D003-simple-get-endpoint-with-routing#diff)

**RSpec Setup & Generating Documentation:**

- [`004-005:` Simple GET Endpoint Tests and Documentation](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/005-simple-get-endpoint-tests-and-documentation%0D004-simple-get-endpoint-with-routing-improvements#diff)

**Fruit (Simple Endpoints):**

- [`005-006:` Fruit Model and Database Migration](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/006-fruit-simple-model-and-database-migration%0D005-simple-get-endpoint-tests-and-documentation#diff)
- [`006-007:` POST /api/v1/fruit with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/007-fruit-POST-with-database-and-validations-and-rspec-tests%0D006-fruit-simple-model-and-database-migration#diff)
- [`007-008:` GET /api/v1/fruits?sort=asc with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/008-fruit-GET-collection-with-sort-and-serializer%0D007-fruit-POST-with-database-and-validations-and-rspec-tests#diff)
- [`008-009:` GET /api/v1/fruits/:id with Rspec and rescue_from for 404s and Suppress JSONAPI Logs](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/009-fruit-GET-single-with-tests-and-rescue_from%0D008-fruit-GET-collection-with-sort-and-serializer#diff)
- [`009-010:` PUT /api/v1/fruits/:id with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/010-fruit-PUT-with-tests%0D009-fruit-GET-single-with-tests-and-rescue_from#diff)
- [`010-011:` DELETE /api/v1/fruits/:id with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/011-fruit-DELETE-with-tests%0D010-fruit-PUT-with-tests#diff) (Note: contains a minor bug that was resolved in `028-029`)


**Seeder:**

- [`011-012:` Seeder](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/012-seeder-started-with-fruit%0D011-fruit-DELETE-with-tests#diff)

**Fruit Cultivars (1-to-N aka One-to-Many Relationships):**

- [`012-013:` Fruit Cultivar Model, Database Migration, Factory, and Seeds with Model RSpec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/013-cultivar-migration-model-factory-seeder-spec%0D012-seeder-started-with-fruit#diff)
- [`013-014:` POST /api/v1/fruits/:fruit_id/fruit_cultivars with Rspec and Suppress Some Default Routes](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/014-cultivar-POST-with-rspec-tests%0D013-cultivar-migration-model-factory-seeder-spec#diff) (Note: suppressing the routes did not work on a mac laptop for some reason)
- [`014-015:` GET /api/v1/fruit_cultivars?sort=asc and GET /api/v1/fruits/:fruit_id/fruit_cultivars?sort=asc with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/015-cultivar-GET-all-or-for-fruit-with-sort-and-specs%0D014-cultivar-POST-with-rspec-tests#diff)
- [`015-016:` GET /api/v1/fruits/:id?include=fruit_cultivars](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/016-fruit-GET-include-fruit-cultivars-with-spec%0D015-cultivar-GET-all-or-for-fruit-with-sort-and-specs#diff)
- [`016-017:` GET /api/v1/fruit_cultivars?include=fruit with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/017-cultivar-GET-include-fruit-with-spec%0D016-fruit-GET-include-fruit-cultivars-with-spec#diff)
- [`017-018:` PUT /api/v1/fruit_cultivars/:id with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/018-cultivar-PUT-with-tests%0D017-cultivar-GET-include-fruit-with-spec#diff)
- [`018-019:` DELETE /api/v1/fruit_cultivars/:id with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/019-cultivar-DELETE-with-tests%0D018-cultivar-PUT-with-tests#diff) (Note: contains a minor bug that was resolved in `028-029`)
- [`019-020:` GET /api/v1/fruit_cultivars/:id?include=fruit with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/020-cultivar-GET-single-with-include-and-tests%0D019-cultivar-DELETE-with-tests#diff)

**Fruit Stands (N-to-M aka Many-to-Many Relationships):**

- [`020-021:` Fruit Stand Model, Database Migrations, Factories, with Model Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/021-stand%2Bfruit-model-migration-spec%0D020-cultivar-GET-single-with-include-and-tests#diff)
- [`021-022:` POST /api/v1/fruit_stands with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/022-stand-POST-with-spec%0D021-stand%2Bfruit-model-migration-spec#diff)
- [`022-023:` POST /api/v1/fruit_stands/:id/fruits with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/023-stand%2Bfruit-POST-add-fruit-with-spec%0D022-stand-POST-with-spec#diff)
- [`023-024:` Fruit Stand Seeder](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/024-stand%2Bfruit-seeder%0D023-stand%2Bfruit-POST-add-fruit-with-spec#diff)
- [`024-025:` GET /api/v1/fruit_stands/:id?include=fruits](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/025-stand%2Bfruit-GET-single-include-with-tests%0D024-stand%2Bfruit-seeder#diff)
- [`025-026:` GET /api/v1/fruit_stands?include=fruits&sort=asc with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/026-stand%2Bfruit-GET-collection-include-sort-with-tests%0D025-stand%2Bfruit-GET-single-include-with-tests#diff)
- [`026-027:` PUT /api/v1/fruit_stands/:id with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/027-stand-PUT-with-tests%0D026-stand%2Bfruit-GET-collection-include-sort-with-tests#diff)
- [`027-028:` DELETE /api/v1/fruit_stands/:id/fruits/:fruit_id with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/028-stand%2Bfruit-DELETE-remove-fruit-with-spec%0D027-stand-PUT-with-tests#diff)
- [`028-029:` DELETE /api/v1/fruit_stands/:id with Rspec and Fix Delete Fruits and Fruit Cultivars](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/029-stand%2Bfruit-DELETE-stand-with-tests-and-fix-other-DELETE-endpoints%0D028-stand%2Bfruit-DELETE-remove-fruit-with-spec#diff)

**Authentication:**

- [`029-030:` POST /api/v1/authenticate - User Log In via JWT with Migration Changes, Model Validation, Email Validator, Password Validator, Factories, and Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/030-auth-POST-authenticate-with-user-migration-model-validations-specs%0D029-stand%2Bfruit-DELETE-stand-with-tests-and-fix-other-DELETE-endpoints#diff)
- [`030-031:` Authentication with Existing Endpoints that Write Data and Rspec Authentication Helper](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/031-auth-require-authentication-for-existing-endpoints-that-write-data%0D030-auth-POST-authenticate-with-user-migration-model-validations-specs#diff)

**Users:**

- [`031-032:` POST /api/v1/users with Migration, Model Updates, Factories, Rspec, and Authentication Blocking Users with Unconfirmed Email Addresses](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/032-user-POST-with-random-email-confirmation-code-and-tests%0D031-auth-require-authentication-for-existing-endpoints-that-write-data#diff)
- [`032-033:` PUT /api/v1/users/:id/confirm_email?code=XXX with Rspec](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/033-user-PUT-confirm-email-address-with-tests%0D032-user-POST-with-random-email-confirmation-code-and-tests) (Note: migration contains error that only lets one user be created; it was fixed in `033-034`)
- [`033-034:` Fix User Table's Index on Email Column](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/034-user-BUGFIX-email-index%0D033-user-PUT-confirm-email-address-with-tests#diff)

**Permissions and Roles with CanCan:**

- [`034-035:` Implement CanCan Permissions and Roles with User Migration, Implemented in FruitsController, Ignored in Other Controllers](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/035-cancan-permissions-and-roles-plus-fruit-controller-updates%0D034-user-BUGFIX-email-index#diff)
- [`035-036:` Implement CanCan Permissions for FruitStand and FruitCultivar](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/036-cancan-fruit_cultivar-fruit_stand-authentication%0D035-cancan-permissions-and-roles-plus-fruit-controller-updates#diff)
- [`036-037:` If Bearer Token is Invalid and Not Empty, Respond to All Requests with 401 Status Code](https://bitbucket.org/bitlather/rails-api-tutorial/branches/compare/037-if-bearer-token-invalid-respond-with-401%0D036-cancan-fruit_cultivar-fruit_stand-authentication#diff)


Notes
=====

Started with this setup tutorial:
```
https://guides.rubyonrails.org/api_app.html
```

Useful commands:

```
$ ruby -v
ruby 2.5.0
$ rails --version
$ sqlite3 --version
```

Start rails app:
```
$ cd api
$ rails server
```

Important files:
```
config/routes.rb
```

Installing Rspec in a project from scratch:
1. Add rspec gem to `Gemfile`
2. Run `$ bundle install`
3. `$ rails generate rspec:install`

Run tests in `api/spec`:
```
$ cd api
$ rspec
```

Running a subset of tests and generating documentation from `api/spec/acceptance`:
```
$ cd api
$ rake docs:generate
# You can view the HTML output in api/doc/api/index.html
```

Running database migrations from `api/db/migrate`:
```
$ cd api
$ bin/rake db:migrate
```

Use the following to run queries directly against your database:
https://sqlitestudio.pl/index.rvt?act=download

- After you install it, open it, click Database > Add a database, then for the file, select the following from this repo's folder: api/db/development.sqlite3

If you want to trash your database and start fresh, which is necessary when you edit migration files that have already been run, delete `api/db/development.sqlite3` and `api/db/test.sqlite3` then re-run `rake db:migrate`.

To seed your `development.sqlite3` database with records from `api/db/seeds.rb`, run:
```
$ rake db:seed
```

You can then verify the records were created with:
```
$ rails server
# Open Postman and hit: GET localhost:3000/api/v1/fruits
# Or, from the command line:
$ curl localhost:3000/api/v1/fruits
```

To list all routes (GET, POST, etc) in your rails applications:
```
$ rails routes
```

Information on the serializer gem we're using:
- http://jsonapi-rb.org/guides/getting_started/rails.html
- http://jsonapi-rb.org/guides/serialization/defining.html

This utility is useful for quickly formatting expectations in rspec:
- https://codebeautify.org/ruby-formatter-beautifier

This utility helps you determine what is wrong when a long rspec response does not match your assertion:
- https://www.diffchecker.com/diff


Creating Records from Console
-----------------------------

- Start rails console:
```
$ rails c
```

- Run ruby commands off of the models to create records (etc):
```
Fruit.create!(name: "Apple")
```


Authentication
--------------

Authentication is based on this tutorial: https://www.pluralsight.com/guides/token-based-authentication-with-ruby-on-rails-5-api

- Add `secret_key_base` to `secrets.yml` and set the value to some Base-64 encoded long string (google a tool online to convert to base 64).

- Create model, migration, factory, etc: `$ rails g model User name email password_digest`

- Add gems

- `$ bundle install`


Roles & Permissions
-------------------

We're using CanCan for roles and permissions.

CanCan Repo:
- https://github.com/CanCanCommunity/cancancan

Role Based Authorization:
- https://github.com/CanCanCommunity/cancancan/wiki/Role-Based-Authorization

Defining Abilities:
- https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
