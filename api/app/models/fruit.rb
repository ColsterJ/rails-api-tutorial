class Fruit < ApplicationRecord

  #
  # If you delete a fruit calling fruit.destroy() then all cultivars that
  # belong to it will also be destroyed.
  #
  has_many :fruit_cultivars, -> { order 'name ASC' }, dependent: :destroy
  has_and_belongs_to_many :fruit_stands

  validates_uniqueness_of :name, :case_sensitive => false # This improves error handling when someone tries to create a fruit with the same name as one that already exists
  validates_length_of :name, :within => 1..100

end
