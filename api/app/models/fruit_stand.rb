class FruitStand < ApplicationRecord

  # By default, sort fruits alphabetically by name
  has_and_belongs_to_many :fruits, -> { order 'name ASC' }

  validates_uniqueness_of :name, :case_sensitive => false # This improves error handling when someone tries to create a fruit with the same name as one that already exists
  validates_length_of :name, :within => 1..100

end
