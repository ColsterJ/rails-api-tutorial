class FruitCultivar < ApplicationRecord

  belongs_to :fruit

  #
  # This improves error handling when someone tries to create a fruit with the
  # same name for the same fruit as one that already exists.
  #
  validates_uniqueness_of :name, :scope => :fruit_id, :case_sensitive => false

  #
  # Name must be 1 to 100 characters long.
  #
  validates_length_of :name, :within => 1..100

end
