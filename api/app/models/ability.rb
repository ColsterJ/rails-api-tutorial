class Ability

  def self.ability_for(user)

    if user.nil?
      Api::V1::GuestAbility.new
    elsif user.is_admin?
      Api::V1::AdminAbility.new(user)
    elsif user.is_super_admin?
      Api::V1::SuperAdminAbility.new(user)
    elsif user.is_user?
      Api::V1::UserAbility.new(user)
    else
      Api::V1::GuestAbility.new
    end

  end

end
