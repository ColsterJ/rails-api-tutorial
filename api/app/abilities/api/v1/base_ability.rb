module Api
  module V1
    class BaseAbility

      include CanCan::Ability

      def initialize(user)
        #
        # These are the things that everyone can do.
        #
        can [:authenticate], :authentication # (no model)
        can [:index, :show], Fruit
        can [:index, :show], FruitCultivar
        can [:index, :show], FruitStand
      end

    end
  end
end
