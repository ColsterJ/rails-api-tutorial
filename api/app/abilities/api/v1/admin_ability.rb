module Api
  module V1
    class AdminAbility < BaseAbility

      def initialize(user)
        super(user)

        can [:create, :destroy, :update], Fruit
        can [:create, :destroy, :update], FruitCultivar
        can [:add_fruit, :create, :destroy, :remove_fruit, :update], FruitStand
      end

    end
  end
end
