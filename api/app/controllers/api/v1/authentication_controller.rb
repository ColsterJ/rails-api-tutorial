module Api
  module V1
    class AuthenticationController < BaseController

      # We're phasing out the old authenticate_request in favor of cancan.
      # Once weve fully implemented cancan in all the controllers, we'll just
      # delete authenticate_request and remove this skip.
      skip_before_action :authenticate_request

      # Authorize with CanCan but don't load any resources.
      # Also don't use the class becuse Authentication does not have a Model.
      authorize_resource :only => [:authenticate], :class => false

      def authenticate
        command = AuthenticateUser.call(params[:email], params[:password])

        if command.success?
          render json: { auth_token: command.result }
        else
          render json: { error: command.errors }, status: :unauthorized
        end
      end
    end
  end
end