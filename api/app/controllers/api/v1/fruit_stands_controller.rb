module Api
  module V1
    class FruitStandsController < BaseController

      # We're phasing out the old authenticate_request in favor of cancan.
      # Once weve fully implemented cancan in all the controllers, we'll just
      # delete authenticate_request and remove this skip.
      skip_before_action :authenticate_request

      # Move user authentication and authorization to cancan.
      load_and_authorize_resource :only => [:add_fruit, :create, :index, :show, :update]

      # Don't load resource on destroy or else we could get 404s on duplicate
      # requests to delete the same row.
      authorize_resource :only => [:destroy, :remove_fruit]

      # Handle 404s.
      rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response

      def add_fruit

        #
        # We use Model.find() to force the models to throw 404 records.
        #
        fruit_stands_fruit = FruitStandsFruit.new(
          fruit_stand: @fruit_stand,
          fruit: Fruit.find(add_fruit_params[:fruit_id])
        )

        #
        # The following would throw uniqueness errors that we want to avoid, so
        # that's why we avoided it:
        #
        #   fruit_stand = FruitStand.find(params[:id])
        #   fruit = Fruit.find(add_fruit_params[:fruit_id])
        #   fruit_stand.fruits << fruit
        #

        #
        # The only thing that can really go wrong at this point is the relationship
        # already exists.
        #
        # However, like with DELETE endpoints, the user just cares that the relationship
        # exists, so they don't need to see an error message about that if they tried to
        # create a duplicate relationship.
        #
        fruit_stands_fruit.save

        render nothing: true, status: :no_content

      end

      def create
       
        fruit_stand = FruitStand.new(
          name: create_params['name'],
        )

        if fruit_stand.save

          render nothing: true, status: :no_content

          #
          # You could also do the following, but symbols like :no_content are preferred:
          #
          #   status: 204
          #
          # See list here:
          # http://billpatrianakos.me/blog/2013/10/13/list-of-rails-status-code-symbols/
          #

        else

          render_jsonapi_errors(fruit_stand)

        end
      end

      def destroy

        #
        # No need to throw 404 if fruit stand does not exist, since user just wants
        # this information gone. So we don't use FruitStand.find() because this
        # would throw a ActiveRecord::RecordNotFound exception and cause the request
        # to return a 404.
        #
        # Also note we have to use "destroy_all" instead of "delete_all".
        #
        #     delete_all just deletes the fruit_stand records.
        #
        #     destroy_all deletes all records that are linked to the fruit_stand too,
        #     as is defined by relationships defined in models, such as
        #     has_and_belongs_to_many.
        #
        FruitStand.where(
          id: params[:id]
        ).destroy_all

        #
        # From https://stackoverflow.com/questions/6439416/deleting-a-resource-using-http-delete
        #
        # "If you DELETE something that doesn't exist, you should just return a 204 (even if the
        # resource never existed). The client wanted the resource gone and it is gone. Returning
        # a 404 is exposing internal processing that is unimportant to the client and will
        # result in an unnecessary error condition."
        #
        render nothing: true, status: :no_content

      end

      def index

        fruit_stands = sorted_fruit_stands || FruitStand.all
        render jsonapi: fruit_stands, include: include_from_params, status: :ok

      end

      def remove_fruit

        #
        # No need to throw 404 if fruit or fruit stand does not exist, since user
        # just wants this information gone. So we don't use Fruit.find() or
        # FruitStand.find(), because those would throw a ActiveRecord::RecordNotFound
        # exception and cause the request to return a 404.
        #
        FruitStandsFruit.where(
          fruit_stand_id: params[:id],
          fruit_id: params[:fruit_id]
        ).delete_all

        #
        # From https://stackoverflow.com/questions/6439416/deleting-a-resource-using-http-delete
        #
        # "If you DELETE something that doesn't exist, you should just return a 204 (even if the
        # resource never existed). The client wanted the resource gone and it is gone. Returning
        # a 404 is exposing internal processing that is unimportant to the client and will
        # result in an unnecessary error condition."
        #
        render nothing: true, status: :no_content        
        
      end

      def show

        render jsonapi: @fruit_stand, include: include_from_params, status: :ok

      end

      def update

        @fruit_stand.name = update_params['name']

        if @fruit_stand.save

          render nothing: true, status: :no_content

        else

          render_jsonapi_errors(@fruit_stand)

        end

      end

      private

        def add_fruit_params
          params.require(:fruit_stands_fruit)
                .permit(
                  :fruit_id
                )
        end

        def create_params
          params.require(:fruit_stand)
                .permit(
                  :name
                )
        end

        def include_from_params
          return [] unless params[:include].present?

          results = []

          #
          # Wrap include param in commas to make finding a string easier.
          #
          includes = ",#{params[:include]},"

          #
          # Push supported resources to the array if they are in the 'include' param.
          #
          results.push(:fruits) if includes.include? ",fruits," # Must be plural because FruitStand model has_and_belongs_to_many :fruits

          #
          # Return an array of things to include.
          #
          results

        end

        def sorted_fruit_stands
          return unless params[:sort].present?
          
          order = params[:sort].casecmp?('desc') ? :desc : :asc
          fruits = FruitStand.order(name: order)
        end

        def update_params
          params.require(:fruit_stand)
                .permit(
                  :name
                )
        end

    end
  end
end
