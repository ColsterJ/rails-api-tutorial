class ApplicationController < ActionController::API

  #
  # HTTP status code symbols for Rails:
  # https://gist.github.com/mlanett/a31c340b132ddefa9cca?fbclid=IwAR2F798BrUnr2GTIYXessOYD-04ElfRftNPUC5qloceIA1CKfXZgKviq-ZA
  #
  before_action :set_current_user
  before_action :authenticate_request
  attr_reader :current_user

  rescue_from InvalidBearerTokenError, with: :render_not_authorized_response

  #
  # Force authorization for all routes with CanCan. You can disable for certain
  # endpoints in a controller with skip_authorization_check.
  #
  # For more information, see:
  #
  #   https://github.com/CanCanCommunity/cancancan/wiki/Ensure-Authorization
  #
  check_authorization

  def current_ability
    @current_ability ||= Ability.ability_for(@current_user)
  end

  def render_json_success(json_param, value)
    render json: {
      "#{json_param}": value
    }.to_json, status: :ok
  end

  def render_jsonapi_errors(object)
    render json: { errors: object.errors }, status: :unprocessable_entity
  end

  private

    def authenticate_request
      render json: { error: 'Not Authorized' }, status: 401 unless @current_user
    end

    def render_not_authorized_response
      render json: { error: 'Not Authorized' }, status: 401
    end

    def set_current_user
      @current_user = AuthorizeApiRequest.call(request.headers).result
    end

end
