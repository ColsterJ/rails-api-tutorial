class SerializableFruitStand < JSONAPI::Serializable::Resource
  # For more information, see http://jsonapi-rb.org/guides/serialization/defining.html
  type 'fruit_stand'

  has_many :fruits

  attributes :name

end
