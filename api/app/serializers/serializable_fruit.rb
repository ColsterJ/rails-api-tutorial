class SerializableFruit < JSONAPI::Serializable::Resource
  # For more information, see http://jsonapi-rb.org/guides/serialization/defining.html
  type 'fruit'

  has_many :fruit_cultivars

  attributes :name

end
