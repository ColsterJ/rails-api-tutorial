module AuthHelper
  def auth_header_for_user(user)

    command = AuthenticateUser.call(user.email, user.password)
    if command.success?
      header 'Authorization', "Bearer #{command.result}"
    else
      raise 'Failed to authorize user for rspec test'
    end

  end
end
