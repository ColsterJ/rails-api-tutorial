FactoryBot.define do
  factory :user do
    first_name { 'Default' }
    last_name { 'Default' }
    email { 'test@localhost.com' }
    password { "MyP@ssword1" }
    email_confirmation_code { '' }
  end

  factory :admin, parent: :user do
    role { 'admin' }
  end

  factory :super_admin, parent: :user do
    role { 'super_admin' }
  end

end
