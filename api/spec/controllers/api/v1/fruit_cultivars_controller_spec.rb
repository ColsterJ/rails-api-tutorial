require 'rails_helper'
require 'rspec_api_documentation/dsl'

#
# This file is basically permissions testing against all user types.
#
# We don't want this in the acceptance folder, because this would
# generate documentation bloat.
#

resource 'FruitCultivar' do

  let(:apple) { FactoryBot.create(:fruit, name: 'Apple') }
  let(:fuji) { FactoryBot.create(:fruit_cultivar, fruit: apple, name: 'Fuji') }

  before do
    header 'Content-Type', 'application/json'
  end

  delete '/api/v1/fruit_cultivars/:id' do

    let(:id) { fuji.id }

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Delete (204)' do
        expect(status).to eq(204)
        expect(FruitCultivar.all.size).to eq(0)
      end
    end

    describe 'guest' do
      example_request 'Delete (403)' do
        expect(status).to eq(403)
        expect(FruitCultivar.all.size).to eq(1)
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Delete (204)' do
        expect(status).to eq(204)
        expect(FruitCultivar.all.size).to eq(0)
      end
    end

    describe 'unauthorized_admin' do
      before { log_in_as_unauthorized_admin }
      example_request 'Delete (401)' do
        expect(status).to eq(401)
        expect(FruitCultivar.all.size).to eq(1)
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Delete (403)' do
        expect(status).to eq(403)
        expect(FruitCultivar.all.size).to eq(1)
      end
    end

  end

  get '/api/v1/fruit_cultivars' do

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Index (200)' do
        expect(status).to eq(200)
      end
    end

    describe 'guest' do
      example_request 'Index (200)' do
        expect(status).to eq(200)
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Index (200)' do
        expect(status).to eq(200)
      end
    end

    describe 'unauthorized_admin' do
      before { log_in_as_unauthorized_admin }
      example_request 'Index (401)' do
        expect(status).to eq(401)
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Index (200)' do
        expect(status).to eq(200)
      end
    end

  end

  get '/api/v1/fruit_cultivars/:id' do

    let(:id) { fuji.id }

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Show (200)' do
        expect(status).to eq(200)
      end
    end

    describe 'guest' do
      example_request 'Show (200)' do
        expect(status).to eq(200)
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Show (200)' do
        expect(status).to eq(200)
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Show (200)' do
        expect(status).to eq(200)
      end
    end

  end

  get '/api/v1/fruits/:fruit_id/fruit_cultivars' do

    let(:fruit_id) { apple.id }

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Index (200)' do
        expect(status).to eq(200)
      end
    end

    describe 'guest' do
      example_request 'Index (200)' do
        expect(status).to eq(200)
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Index (200)' do
        expect(status).to eq(200)
      end
    end

    describe 'unauthorized_admin' do
      before { log_in_as_unauthorized_admin }
      example_request 'Index (401)' do
        expect(status).to eq(401)
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Index (200)' do
        expect(status).to eq(200)
      end
    end

  end

  post '/api/v1/fruits/:fruit_id/fruit_cultivars' do

    let(:fruit_id) { apple.id }

    let(:raw_post) do
      {
        fruit_cultivar: {
          name: 'Gala',
        }
      }.to_json
    end

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Post (204)' do
        expect(status).to eq(204)
        expect(FruitCultivar.first.name).to eq('Gala')
      end
    end

    describe 'guest' do
      example_request 'Post (403)' do
        expect(status).to eq(403)
        expect(FruitCultivar.all.size).to eq(0)
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Post (204)' do
        expect(status).to eq(204)
        expect(FruitCultivar.first.name).to eq('Gala')
      end
    end

    describe 'unauthorized_admin' do
      before { log_in_as_unauthorized_admin }
      example_request 'Post (401)' do
        expect(status).to eq(401)
        expect(FruitCultivar.all.size).to eq(0)
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Post (403)' do
        expect(status).to eq(403)
        expect(FruitCultivar.all.size).to eq(0)
      end
    end

  end

  put '/api/v1/fruit_cultivars/:id' do

    let(:id) { fuji.id }

    let(:raw_post) do
      {
        fruit_cultivar: {
          name: 'Gala'
        }
      }.to_json
    end

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Put (204)' do
        expect(status).to eq(204)
        expect(FruitCultivar.find(fuji.id).name).to eq('Gala')
      end
    end

    describe 'guest' do
      example_request 'Put (403)' do
        expect(status).to eq(403)
        expect(FruitCultivar.find(fuji.id).name).to eq('Fuji')
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Put (204)' do
        expect(status).to eq(204)
        expect(FruitCultivar.find(fuji.id).name).to eq('Gala')
      end
    end

    describe 'unauthorized_admin' do
      before { log_in_as_unauthorized_admin }
      example_request 'Put (401)' do
        expect(status).to eq(401)
        expect(FruitCultivar.find(fuji.id).name).to eq('Fuji')
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Put (403)' do
        expect(status).to eq(403)
        expect(FruitCultivar.find(fuji.id).name).to eq('Fuji')
      end
    end

  end

end