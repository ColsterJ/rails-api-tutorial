require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource 'User Authentication' do

  before(:each) do
    header 'Content-Type', 'application/json'
  end

  post '/api/v1/authenticate' do

    let(:user) { FactoryBot.create(:user, email: 'other_user@localhost.com') }

    let(:raw_post) do
      {
        email: user.email,
        password: user.password
      }.to_json
    end

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Post (200)' do
        expect(status).to eq(200)
        expect(JSON.parse(response_body)).to have_key('auth_token')
      end
    end

    describe 'guest' do
      example_request 'Post (200)' do
        expect(status).to eq(200)
        expect(JSON.parse(response_body)).to have_key('auth_token')
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Post (200)' do
        expect(status).to eq(200)
        expect(JSON.parse(response_body)).to have_key('auth_token')
      end
    end

    describe 'unauthorized_admin' do
      before { log_in_as_unauthorized_admin }
      example_request 'Post (401)' do
        expect(status).to eq(401)
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Post (200)' do
        expect(status).to eq(200)
        expect(JSON.parse(response_body)).to have_key('auth_token')
      end
    end

  end
end
