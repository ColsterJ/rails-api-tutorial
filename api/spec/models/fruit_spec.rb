require 'rails_helper'

RSpec.describe Fruit, type: :model do

  it "destroys cultivars when it is destroyed" do

    apple = FactoryBot.create(:fruit, name: 'Apple')
    orange = FactoryBot.create(:fruit, name: 'Orange')

    FactoryBot.create(:fruit_cultivar, fruit: apple, name: 'Fuji')
    FactoryBot.create(:fruit_cultivar, fruit: apple, name: 'Gala')
    FactoryBot.create(:fruit_cultivar, fruit: orange, name: 'Blood Orange')

    expect(FruitCultivar.all.size).to eq(3)

    apple.destroy

    expect(FruitCultivar.all.size).to eq(1)

  end

end
