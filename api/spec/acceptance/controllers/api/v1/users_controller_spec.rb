require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource 'User' do

  explanation "CRUD actions for the user table. On success, an email is sent to the new user with a link to confirm their email address (Note: email is not implemented yet)."

  before(:each) do
    header 'Content-Type', 'application/json'
  end

  post '/api/v1/users' do
    with_options scope: :user do
      parameter :first_name, "User's first name", required: true
      parameter :last_name, "User's last name", required: true
      parameter :email, "Email address, which will be used to log in", required: true
      parameter :password, "Unencrypted password", required: true
    end

    context '204' do

      let(:raw_post) do
        {
          user: {
            first_name: 'Ruth',
            last_name: 'Bader Ginsburg',
            email: 'ruth@scotus.us',
            password: 'N0toriou$:RBG'
          }
        }.to_json
      end

      example_request 'Create a User (204)' do

        expect(status).to eq(204)

        expect(User.all.size).to eq(1)
        expect(User.first.email_confirmation_code.length).to be 8
 
      end
    end

    context '422' do

      before do
        FactoryBot.create(:user, email: 'RUTH@SCOTUS.US')
      end

      let(:raw_post) do
        {
          user: {
            first_name: 'Ruth',
            last_name: 'Bader Ginsburg',
            email: 'ruth@scotus.us',
            password: 'N0toriou$:RBG'
          }
        }.to_json
      end

      example_request 'Create a User (422: Email Is Not Unique)' do

        expect(status).to eq(422)

        expected_response = { "errors" => { "email" => [ "has already been taken" ] } }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(User.all.size).to eq(1)
 
      end
    end

    context '422' do

      let(:raw_post) do
        {
          user: {
            not_an_expected_param: 'Garbage'
          }
        }.to_json
      end

      example_request 'Create a User (422: Missing Arguments)' do

        expect(status).to eq(422)

        expect(User.all.size).to eq(0)

         expected_response = {
          "errors" => {
            "email" => [
              "can't be blank",
              "is not a valid email address",
              "is too short (minimum is 5 characters)"
            ],
            "first_name" => ["is too short (minimum is 1 character)"],
            "last_name" => ["is too short (minimum is 1 character)"],
            "password" => [
              "can't be blank",
              "length must be between 6 and 72 characters long",
              "must contain a number (0-9)",
              "must contain a symbol (!@$ etc)"
            ]
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '422' do

      let(:raw_post) do
        {
          user: {
            first_name: Array.new(101){ 'a' }.join,
            last_name: Array.new(101){ 'a' }.join,
            email: 'ruth@scotus.us' + Array.new(201){ 'a' }.join,
            password: 'N0toriou$:RBG' + Array.new(101){ 'a' }.join
          }
        }.to_json
      end

      example_request 'Create a User (422: Arguments Are Too Long)' do

        expect(status).to eq(422)

        expect(User.all.size).to eq(0)

        expected_response = {
          "errors" => {
            "password" => [
              "is too long (maximum is 72 characters)",
              "length must be between 6 and 72 characters long"
            ],
            "first_name" => ["is too long (maximum is 100 characters)"],
            "last_name" => ["is too long (maximum is 100 characters)"],
            "email" => ["is too long (maximum is 200 characters)"]
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

  end

  put '/api/v1/users/:id/confirm_email?code=:code' do
    parameter :code, "Confirmation code", required: true

    context '204' do

      before do
        @user = FactoryBot.create(:user, email_confirmation_code: 'ZbDFQr')
      end

      let(:id) { @user.id }
      let(:code) { @user.email_confirmation_code }

      example_request "Confirm a User's Email (204)" do
        expect(status).to eq(204)
        expect(User.find(@user.id).email_confirmation_code).to eq('')
      end

    end

    context '204' do

      before do
        @user = FactoryBot.create(:user, email_confirmation_code: '')
      end

      let(:id) { @user.id }
      let(:code) { 'ZZZZZ' }

      example_request "Confirm a User's Email (204: User Already Confirmed)" do
        expect(status).to eq(204)
        expect(User.find(@user.id).email_confirmation_code).to eq('')
      end

    end

    context '404' do

      let(:id) { 999 }
      let(:code) { 'ZZZZZ' }

      example_request "Confirm a User's Email (404: User Not Found)" do
        expect(status).to eq(404)

        expected_response = {
          "errors" => [
            {
              "status" => "404",
              "title" => "Record not found",
              "detail" => "Couldn't find User with 'id'=999"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)
      end

    end

    context '422' do

      before do
        @user = FactoryBot.create(:user, email_confirmation_code: 'ZbDFQr')
      end

      let(:id) { @user.id }
      let(:code) { 'ZZZZZZ' }

      example_request "Confirm a User's Email (422: Wrong Code)" do
        expect(status).to eq(422)

        expected_response = {
          "errors" => [
            {
              "detail" => "Code did not match"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)
      end

    end

  end

end
