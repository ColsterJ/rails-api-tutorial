require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource 'Fruit Stand' do

  explanation "Examples of N-to-M (aka 'many-to-many') database relationships and their resources."

  before(:each) do
    header 'Content-Type', 'application/json'
  end

  delete '/api/v1/fruit_stands/:id' do

    parameter :id, "Fruit Stand's ID", required: true

    context '204' do
    
      before do

        log_in_as_admin

        @fruit_stand = FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")

        @orange = FactoryBot.create(:fruit, name: 'Orange')
        @apple = FactoryBot.create(:fruit, name: 'Apple')

        @fruit_stand.fruits << @orange
        @fruit_stand.fruits << @apple

      end

      let(:id) { @fruit_stand.id }
      let(:fruit_id) { @apple.id }

      example_request 'Delete a Fruit Stand (204)' do

        expect(status).to eq(204)

        expect(FruitStandsFruit.all.size).to eq(0)
        expect(FruitStand.all.size).to eq(0)
        expect(Fruit.all.size).to eq(2)

      end
    end

    context '204' do

      before do

        log_in_as_admin

      end

      let(:id) { 999 }
      let(:fruit_id) { 999 }

      example_request 'Delete a Fruit Stand (204: Records Were Already Deleted)' do

        expect(status).to eq(204)

        expect(FruitStandsFruit.all.size).to eq(0)
        expect(Fruit.all.size).to eq(0)

      end
    end

    context '403' do

      before do

        @fruit_stand = FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")

        @orange = FactoryBot.create(:fruit, name: 'Orange')
        @apple = FactoryBot.create(:fruit, name: 'Apple')

        @fruit_stand.fruits << @orange
        @fruit_stand.fruits << @apple

      end

      let(:id) { @fruit_stand.id }
      let(:fruit_id) { @apple.id }

      example_request 'Delete a Fruit Stand (403: Access Denied)' do

        expect(status).to eq(403)

        expected_response = {
          "errors" => [
            {
              "detail" => "You are not authorized to access this page.",
              "status" => "403",
              "title" => "Access Denied"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitStandsFruit.all.size).to eq(2)
        expect(FruitStand.all.size).to eq(1)
        expect(Fruit.all.size).to eq(2)

      end
    end

  end

  delete '/api/v1/fruit_stands/:id/fruits/:fruit_id' do

    parameter :id, "Fruit Stand's ID", required: true
    parameter :fruit_id, "Fruit's ID", required: true

    context '204' do
    
      before do

        log_in_as_admin

        @fruit_stand = FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")

        @orange = FactoryBot.create(:fruit, name: 'Orange')
        @apple = FactoryBot.create(:fruit, name: 'Apple')

        @fruit_stand.fruits << @orange
        @fruit_stand.fruits << @apple

      end

      let(:id) { @fruit_stand.id }
      let(:fruit_id) { @apple.id }

      example_request 'Remove a Fruit from a Fruit Stand (204)' do

        expect(status).to eq(204)

        expect(FruitStandsFruit.all.size).to eq(1)
        expect(Fruit.all.size).to eq(2)

      end
    end

    context '204' do

      before do

        log_in_as_admin

      end

      let(:id) { 999 }
      let(:fruit_id) { 999 }

      example_request 'Remove a Fruit from a Fruit Stand (204: Records Were Already Deleted)' do

        expect(status).to eq(204)

        expect(FruitStandsFruit.all.size).to eq(0)
        expect(Fruit.all.size).to eq(0)

      end
    end

    context '403' do
    
      before do

        @fruit_stand = FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")

        @orange = FactoryBot.create(:fruit, name: 'Orange')
        @apple = FactoryBot.create(:fruit, name: 'Apple')

        @fruit_stand.fruits << @orange
        @fruit_stand.fruits << @apple

      end

      let(:id) { @fruit_stand.id }
      let(:fruit_id) { @apple.id }

      example_request 'Remove a Fruit from a Fruit Stand (403: Access Denied)' do

        expect(status).to eq(403)

        expected_response = {
          "errors" => [
            {
              "detail" => "You are not authorized to access this page.",
              "status" => "403",
              "title" => "Access Denied"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitStandsFruit.all.size).to eq(2)
        expect(Fruit.all.size).to eq(2)

      end
    end

  end

  get '/api/v1/fruit_stands/:id' do

    parameter :id, "Record's ID", required: true

    context '200' do
    
      before do
        @fruit_stand = FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")
        @fruit_stand.fruits << FactoryBot.create(:fruit, name: 'Orange')
        @fruit_stand.fruits << FactoryBot.create(:fruit, name: 'Apple')
      end

      let(:id) { @fruit_stand.id }

      example_request 'Get a Fruit Stand (200)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => {
            "attributes" => {
              "name" => "Ollie's Orchard"
            },
            "id" => @fruit_stand.id.to_s,
            "type" => "fruit_stand",
            "relationships" => { "fruits" => { "meta" => { "included" => false } } }
          },
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '404' do

      let(:id) { 999 }

      example_request 'Get a Fruit Stand (404)' do

        expect(status).to eq(404)

        expected_response = {
          "errors" => [
            {
              "detail" => "Couldn't find FruitStand with 'id'=999",
              "status" => "404",
              "title" => "Record not found"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end

    end
  end

  get '/api/v1/fruit_stands/:id?include=:relationship' do

    parameter :id, "Record's ID", required: true
    parameter :relationship, "Model (must be related to record)"

    context '200' do

      before do
        @fruit_stand = FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")

        @orange = FactoryBot.create(:fruit, name: 'Orange')
        @apple = FactoryBot.create(:fruit, name: 'Apple')
        
        @fruit_stand.fruits << @orange
        @fruit_stand.fruits << @apple
      end

      let(:id) { @fruit_stand.id }
      let(:relationship) { 'fruits' }

      example_request 'Get a Fruit Stand with Fruits (200)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => {
            "attributes" => {
              "name" => "Ollie's Orchard"
            },
            "id" => @fruit_stand.id.to_s,
            "type" => "fruit_stand",
            "relationships" => {
              "fruits" => {
                "data" => [{
                  "id" => @apple.id.to_s,
                  "type" => "fruit"
                }, {
                  "id" => @orange.id.to_s,
                  "type" => "fruit"
                }]
              }
            },
          },
          "included" => [{
            "id" => @apple.id.to_s,
            "type" => "fruit",
            "attributes" => {
              "name" => "Apple"
            },
            "relationships" => {
              "fruit_cultivars" => {
                "meta" => {
                  "included" => false
                }
              }
            }
          }, {
            "id" => @orange.id.to_s,
            "type" => "fruit",
            "attributes" => {
              "name" => "Orange"
            },
            "relationships" => {
              "fruit_cultivars" => {
                "meta" => {
                  "included" => false
                }
              }
            }
          }],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end
  end

  get '/api/v1/fruit_stands' do

    context '200' do

      before do
        @apples_r_us = FactoryBot.create(:fruit_stand, name: 'Apples R Us')
        @oranges_r_us = FactoryBot.create(:fruit_stand, name: 'Oranges R Us')
        @bananas_r_us = FactoryBot.create(:fruit_stand, name: 'Bananas R Us')
      end

      example_request 'Get Fruit Stands (200)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "name" => "Apples R Us"
              },
              "id" => @apples_r_us.id.to_s,
              "type" => "fruit_stand",
              "relationships" => { "fruits" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Oranges R Us"
              },
              "id" => @oranges_r_us.id.to_s,
              "type" => "fruit_stand",
              "relationships" => { "fruits" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Bananas R Us"
              },
              "id" => @bananas_r_us.id.to_s,
              "type" => "fruit_stand",
              "relationships" => { "fruits" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end
  end

  get '/api/v1/fruit_stands?sort=:sort' do

    parameter :sort, 'Sort by name (asc or desc)', required: false

    before do
      @apples_r_us = FactoryBot.create(:fruit_stand, name: 'Apples R Us')
      @oranges_r_us = FactoryBot.create(:fruit_stand, name: 'Oranges R Us')
      @bananas_r_us = FactoryBot.create(:fruit_stand, name: 'Bananas R Us')
    end

    context '200' do

      let(:sort) { 'asc' }

      example_request 'Get Fruit Stands (200: Sort By Name, Ascending)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "name" => "Apples R Us"
              },
              "id" => @apples_r_us.id.to_s,
              "type" => "fruit_stand",
              "relationships" => { "fruits" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Bananas R Us"
              },
              "id" => @bananas_r_us.id.to_s,
              "type" => "fruit_stand",
              "relationships" => { "fruits" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Oranges R Us"
              },
              "id" => @oranges_r_us.id.to_s,
              "type" => "fruit_stand",
              "relationships" => { "fruits" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '200' do

      let(:sort) { 'desc' }

      example_request 'Get Fruit Stands (200: Sort By Name, Descending)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "name" => "Oranges R Us"
              },
              "id" => @oranges_r_us.id.to_s,
              "type" => "fruit_stand",
              "relationships" => { "fruits" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Bananas R Us"
              },
              "id" => @bananas_r_us.id.to_s,
              "type" => "fruit_stand",
              "relationships" => { "fruits" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Apples R Us"
              },
              "id" => @apples_r_us.id.to_s,
              "type" => "fruit_stand",
              "relationships" => { "fruits" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '200' do

      let(:sort) { 'BAD_VALUE' }

      example_request 'Get Fruit Stands (200: Sort By Name, Bad Value)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "name" => "Apples R Us"
              },
              "id" => @apples_r_us.id.to_s,
              "type" => "fruit_stand",
              "relationships" => { "fruits" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Bananas R Us"
              },
              "id" => @bananas_r_us.id.to_s,
              "type" => "fruit_stand",
              "relationships" => { "fruits" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Oranges R Us"
              },
              "id" => @oranges_r_us.id.to_s,
              "type" => "fruit_stand",
              "relationships" => { "fruits" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end
  end

  get '/api/v1/fruit_stands?include=:relationship' do

    parameter :relationship, "Model (must be related to record)"

    context '200' do

      before do

        @apple = FactoryBot.create(:fruit, name: 'Apple')
        @pear = FactoryBot.create(:fruit, name: 'Pear')
        @orange = FactoryBot.create(:fruit, name: 'Orange')

        @apples_r_us = FactoryBot.create(:fruit_stand, name: 'Apples R Us')
        @oranges_r_us = FactoryBot.create(:fruit_stand, name: 'Oranges R Us')
        @bananas_r_us = FactoryBot.create(:fruit_stand, name: 'Bananas R Us')

        @apples_r_us.fruits << @apple
        @apples_r_us.fruits << @pear
        @oranges_r_us.fruits << @orange

      end

      let(:relationship) { 'fruits' }

      example_request 'Get Fruit Stands with Fruits (200)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
            "attributes" => {
              "name" => "Apples R Us"
            },
            "id" => @apples_r_us.id.to_s,
            "relationships" => {
              "fruits" => {
                "data" => [{
                  "id" => @apple.id.to_s,
                  "type" => "fruit",
                }, {
                  "id" => @pear.id.to_s,
                  "type" => "fruit"
                }]
              }
            },
            "type" => "fruit_stand"
          }, {
            "attributes" => {
              "name" => "Oranges R Us"
            },
            "id" => @oranges_r_us.id.to_s,
            "relationships" => {
              "fruits" => {
                "data" => [{
                  "id" => @orange.id.to_s,
                  "type" => "fruit"
                }]
              }
            },
            "type" => "fruit_stand"
          }, {
            "attributes" => {
              "name" => "Bananas R Us"
            },
            "id" => @bananas_r_us.id.to_s,
            "relationships" => {
              "fruits" => {
                "data" => []
              }
            },
            "type" => "fruit_stand"
          }],
          "included" => [{
            "attributes" => {
              "name" => "Apple"
            },
            "id" => @apple.id.to_s,
            "type" => "fruit",
            "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
          }, {
            "attributes" => {
              "name" => "Pear"
            },
            "id" => @pear.id.to_s,
            "type" => "fruit",
            "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
          }, {
            "attributes" => {
              "name" => "Orange"
            },
            "id" => @orange.id.to_s,
            "type" => "fruit",
            "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
          }],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end
  end

  post '/api/v1/fruit_stands/:id/fruits' do
    with_options scope: :fruit_stands_fruit do
      parameter :fruit_id, 'The fruit you want to add to the fruit stand', required: true
    end

    context '204' do

      before do

        log_in_as_admin

        @fruit_stand = FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")
        @fruit = FactoryBot.create(:fruit, name: "Apple")

      end

      let(:id) { @fruit_stand.id }

      let(:raw_post) do
        {
          fruit_stands_fruit: {
            fruit_id: @fruit.id
          }
        }.to_json
      end

      example_request 'Add a Fruit to a Fruit Stand (204)' do

        expect(status).to eq(204)

        expect(FruitStandsFruit.all.size).to eq(1)
 
      end
    end

    context '403' do

      before do

        @fruit_stand = FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")
        @fruit = FactoryBot.create(:fruit, name: "Apple")

      end

      let(:id) { @fruit_stand.id }

      let(:raw_post) do
        {
          fruit_stands_fruit: {
            fruit_id: @fruit.id
          }
        }.to_json
      end

      example_request 'Add a Fruit to a Fruit Stand (403: Access Denied)' do

        expect(status).to eq(403)

        expected_response = {
          "errors" => [
            {
              "detail" => "You are not authorized to access this page.",
              "status" => "403",
              "title" => "Access Denied"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitStandsFruit.all.size).to eq(0)
 
      end
    end

    context '404' do

      before do

        user = FactoryBot.create(:user)
        auth_header_for_user(user)

        @fruit = FactoryBot.create(:fruit, name: "Apple")

      end

      let(:id) { 999 }

      let(:raw_post) do
        {
          fruit_stands_fruit: {
            fruit_id: @fruit.id
          }
        }.to_json
      end

      example_request 'Add a Fruit to a Fruit Stand (404: Fruit Stand Not Found)' do

        expect(status).to eq(404)

        expected_response = {
          "errors" => [{
            "detail" => "Couldn't find FruitStand with 'id'=999",
            "status" => "404",
            "title" => "Record not found"
          }]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitStandsFruit.all.size).to eq(0)
 
      end
    end

    context '404' do

      before do

        log_in_as_admin

        @fruit_stand = FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")

      end

      let(:id) { @fruit_stand.id }

      let(:raw_post) do
        {
          fruit_stands_fruit: {
            fruit_id: 999
          }
        }.to_json
      end

      example_request 'Add a Fruit to a Fruit Stand (404: Fruit Not Found)' do

        expect(status).to eq(404)

        expected_response = {
          "errors" => [{
            "detail" => "Couldn't find Fruit with 'id'=999",
            "status" => "404",
            "title" => "Record not found"
          }]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitStandsFruit.all.size).to eq(0)
 
      end
    end

    context '422' do

      before do

        log_in_as_admin

        @fruit_stand = FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")
        @fruit = FactoryBot.create(:fruit, name: "Apple")
        @fruit_stand.fruits << @fruit

      end

      let(:id) { @fruit_stand.id }

      let(:raw_post) do
        {
          fruit_stands_fruit: {
            fruit_id: @fruit.id
          }
        }.to_json
      end

      example_request 'Add a Fruit to a Fruit Stand (204: Relationship Already Exists)' do

        expect(status).to eq(204)

        expect(FruitStandsFruit.all.size).to eq(1)
 
      end
    end

  end

  post '/api/v1/fruit_stands' do
    with_options scope: :fruit_stand do
      parameter :name, 'Name of the fruit stand you want to create', required: true
    end

    context '204' do

      before do

        log_in_as_admin

      end

      let(:raw_post) do
        {
          fruit_stand: {
            name: "Ollie's Orchard"
          }
        }.to_json
      end

      example_request 'Create a Fruit Stand (204)' do

        expect(status).to eq(204)

        expect(FruitStand.all.size).to eq(1)
 
      end
    end

    context '401' do

      let(:raw_post) do
        {
          fruit_stand: {
            name: "Ollie's Orchard"
          }
        }.to_json
      end

      example_request 'Create a Fruit Stand (403: Access Denied)' do

        expect(status).to eq(403)

        expected_response = {
          "errors" => [
            {
              "detail" => "You are not authorized to access this page.",
              "status" => "403",
              "title" => "Access Denied"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitStand.all.size).to eq(0)
 
      end
    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:raw_post) do
        {
          fruit_stand: {
            not_an_expected_param: 'Garbage'
          }
        }.to_json
      end

      example_request 'Create a Fruit Stand (422: Name Is Missing)' do

        expect(status).to eq(422)

        expect(FruitStand.all.size).to eq(0)

         expected_response = {
          "errors" => { "name" => ["is too short (minimum is 1 character)"] }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:raw_post) do
        {
          fruit_stand: {
            name: Array.new(101){ 'a' }.join
          }
        }.to_json
      end

      example_request 'Create a Fruit Stand (422: Name Is Too Long)' do

        expect(status).to eq(422)

        expect(FruitStand.all.size).to eq(0)

         expected_response = {
          "errors" => { "name" => ["is too long (maximum is 100 characters)"] }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '422' do

      before do

        log_in_as_admin

        FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")

      end

      let(:raw_post) do
        {
          fruit_stand: {
            name: "oLLIE'S oRCHARD" # Test case-insensitive while we're at it
          }
        }.to_json
      end

      example_request 'Create a Fruit Stand (422: Name Is Not Unique)' do

        expect(status).to eq(422)

        expected_response = {
          "errors" => { "name" => ["has already been taken"] }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitStand.all.size).to eq(1)

      end
    end

  end

  put '/api/v1/fruit_stands/:id' do

    parameter :id, "Record's ID", required: true
    with_options scope: :fruit_stand do
      parameter :name, 'New name', required: true
    end

    context '204' do

      before do

        log_in_as_admin

      end

      let(:fruit_stand) { FactoryBot.create(:fruit_stand, name: 'Apples R Us') }
      let(:id) { fruit_stand.id }
      let(:raw_post) do
        {
          fruit_stand: {
            name: 'Bananas R Us'
          }
        }.to_json
      end

      example_request 'Update a Fruit Stand (204)' do

        expect(status).to eq(204)

        expect(FruitStand.all.size).to eq(1)

        expect(FruitStand.first['name']).to eq('Bananas R Us')

      end

    end

    context '403' do

      let(:fruit_stand) { FactoryBot.create(:fruit_stand, name: 'Apples R Us') }
      let(:id) { fruit_stand.id }
      let(:raw_post) do
        {
          fruit_stand: {
            name: 'Bananas R Us'
          }
        }.to_json
      end

      example_request 'Update a Fruit Stand (403: Access Failed)' do

        expect(status).to eq(403)

        expected_response = {
          "errors" => [
            {
              "detail" => "You are not authorized to access this page.",
              "status" => "403",
              "title" => "Access Denied"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitStand.all.size).to eq(1)

        expect(FruitStand.first['name']).to eq('Apples R Us')

      end

    end

    context '404' do

      before do

        user = FactoryBot.create(:user)
        auth_header_for_user(user)

      end

      let(:id) { 999 }

      example_request 'Update a Fruit Stand (404)' do

        expect(status).to eq(404)

          expected_response = {
            "errors" => [
              {
                "status" => "404",
                "title" => "Record not found",
                "detail" => "Couldn't find FruitStand with 'id'=999"
              }
            ]
          }

          expect(JSON.parse(response_body)).to eq(expected_response)

      end

    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:fruit) { FactoryBot.create(:fruit_stand, name: 'Apples R Us') }
      let(:id) { fruit.id }
      let(:raw_post) do
        {
          fruit_stand: {
            name: ''
          }
        }.to_json
      end

      example_request 'Update a Fruit Stand (422: Name Is Too Short)' do

        expect(status).to eq(422)

        expected_response = {
          "errors" => {"name"=>["is too short (minimum is 1 character)"]}
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitStand.all.size).to eq(1)

        expect(FruitStand.first['name']).to eq('Apples R Us')

      end

    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:fruit_stand) { FactoryBot.create(:fruit_stand, name: 'Apples R Us') }
      let(:id) { fruit_stand.id }
      let(:raw_post) do
        {
          fruit_stand: {
            not_an_expected_param: 'Garbage'
          }
        }.to_json
      end

      example_request 'Update a Fruit Stand (422: Name Is Missing)' do

        expect(status).to eq(422)

        expected_response = {
          "errors" => {"name"=>["is too short (minimum is 1 character)"]}
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitStand.all.size).to eq(1)

        expect(FruitStand.first['name']).to eq('Apples R Us')

      end

    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:fruit_stand) { FactoryBot.create(:fruit_stand, name: 'Apples R Us') }
      before { FactoryBot.create(:fruit_stand, name: 'Bananas R Us') }
      let(:id) { fruit_stand.id }
      let(:raw_post) do
        {
          fruit_stand: {
            name: 'Bananas R Us'
          }
        }.to_json
      end

      example_request 'Update a Fruit Stand (422: Name Is Not Unique)' do

        expect(status).to eq(422)

        expected_response = {
          "errors" => {"name"=>["has already been taken"]}
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitStand.all.size).to eq(2)

      end

    end

  end

end
