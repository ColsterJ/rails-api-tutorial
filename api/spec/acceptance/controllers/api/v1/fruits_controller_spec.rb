require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource 'Fruit' do

  explanation "The basics of POST, PUT, GET, DELETE with a database."

  before(:each) do
    header 'Content-Type', 'application/json'
  end

  delete '/api/v1/fruits/:id' do

    parameter :id, "Record's ID", required: true

    context '204' do

      before do

        log_in_as_admin

        @apple = FactoryBot.create(:fruit, name: 'Apple')
        FactoryBot.create(:fruit_cultivar, fruit: @apple, name: 'Fuji')

        orange = FactoryBot.create(:fruit, name: 'Orange')
        FactoryBot.create(:fruit_cultivar, fruit: orange, name: 'Blood Orange')

        fruit_stand = FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")
        fruit_stand.fruits << @apple

      end
      
      let(:id) { @apple.id }

      example_request 'Delete a Fruit (204)' do

        expect(status).to eq(204)

        #
        # Assert only apple and it's cultivar was destroyed.
        #
        expect(Fruit.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitStandsFruit.all.size).to eq(0)
 
      end
    end

    context '403' do

      before do

        @apple = FactoryBot.create(:fruit, name: 'Apple')
        FactoryBot.create(:fruit_cultivar, fruit: @apple, name: 'Fuji')

        orange = FactoryBot.create(:fruit, name: 'Orange')
        FactoryBot.create(:fruit_cultivar, fruit: orange, name: 'Blood Orange')

        fruit_stand = FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")
        fruit_stand.fruits << @apple
      end
      
      let(:id) { @apple.id }

      example_request 'Delete a Fruit (403: Access Denied)' do

        expect(status).to eq(403)

        expected_response = {
          "errors" => [
            {
              "detail" => "You are not authorized to access this page.",
              "status" => "403",
              "title" => "Access Denied"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        #
        # Assert only apple and it's cultivar was destroyed.
        #
        expect(Fruit.all.size).to eq(2)
        expect(FruitCultivar.all.size).to eq(2)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitStandsFruit.all.size).to eq(1)
 
      end
    end

    context '204' do

      before do

        log_in_as_admin

      end

      let(:id) { 999 }

      example_request 'Delete a Fruit (204: Records Were Already Deleted)' do

        expect(status).to eq(204)
 
      end
    end

  end

  get '/api/v1/fruits' do

    context '200' do

      before do
        FactoryBot.create(:fruit, name: 'Apple')
        FactoryBot.create(:fruit, name: 'Orange')
        FactoryBot.create(:fruit, name: 'Banana')
      end

      example_request 'Get Fruits (200)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "name" => "Apple"
              },
              "id" => "1",
              "type" => "fruit",
              "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Orange"
              },
              "id" => "2",
              "type" => "fruit",
              "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Banana"
              },
              "id" => "3",
              "type" => "fruit",
              "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)
 
      end
    end
  end

  #
  # For more information on how to write specs for documentation, see:
  #
  #     https://github.com/zipmark/rspec_api_documentation
  #
  get '/api/v1/fruits?sort=:sort' do

    parameter :sort, 'Sort by name (asc or desc)', required: false

    before do
      FactoryBot.create(:fruit, name: 'Apple')
      FactoryBot.create(:fruit, name: 'Orange')
      FactoryBot.create(:fruit, name: 'Banana')
    end

    context '200' do

      let(:sort) { 'asc' }

      example_request 'Get Fruits (200: Sort By Name, Ascending)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "name" => "Apple"
              },
              "id" => "1",
              "type" => "fruit",
              "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Banana"
              },
              "id" => "3",
              "type" => "fruit",
              "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Orange"
              },
              "id" => "2",
              "type" => "fruit",
              "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)
 
      end
    end

    context '200' do

      let(:sort) { 'desc' }

      example_request 'Get Fruits (200: Sort By Name, Descending)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "name" => "Orange"
              },
              "id" => "2",
              "type" => "fruit",
              "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Banana"
              },
              "id" => "3",
              "type" => "fruit",
              "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Apple"
              },
              "id" => "1",
              "type" => "fruit",
              "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)
 
      end
    end


    context '200' do

      let(:sort) { 'BAD_VALUE' }

      example_request 'Get Fruits (200: Sort By Name, Bad Value)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "name" => "Apple"
              },
              "id" => "1",
              "type" => "fruit",
              "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Banana"
              },
              "id" => "3",
              "type" => "fruit",
              "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "name" => "Orange"
              },
              "id" => "2",
              "type" => "fruit",
              "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)
 
      end
    end
  end

  get '/api/v1/fruits/:id' do

    parameter :id, "Record's ID", required: true

    context '200' do
    
      before do
        FactoryBot.create(:fruit, id: 2, name: 'Orange')
      end

      let(:id) { 2 }

      example_request 'Get a Fruit (200)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => {
            "attributes" => {
              "name" => "Orange"
            },
            "id" => "2",
            "type" => "fruit",
            "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
          },
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '404' do

      let(:id) { 999 }

      example_request 'Get a Fruit (404)' do

        expect(status).to eq(404)

        expected_response = {
          "errors" => [
            {
              "detail" => "Couldn't find Fruit with 'id'=999",
              "status" => "404",
              "title" => "Record not found"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end

    end
  end

  get '/api/v1/fruits/:id?include=:relationship' do

    parameter :id, "Record's ID", required: true
    parameter :relationship, "Model (must be related to record)"

    context '200' do
    
      before do
        @apple = FactoryBot.create(:fruit, id: 1, name: 'Apple')
        @gala = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Gala")
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Fuji")
      end

      let(:id) { 1 }
      let(:relationship) { 'fruit_cultivars' }

      example_request 'Get a Fruit with Fruit Cultivars (200)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => {
            "attributes" => {
              "name" => "Apple"
            },
            "id" => "1",
            "type" => "fruit",
            "relationships" => {
              "fruit_cultivars" => {
                "data" => [{
                  "id" => "2",
                  "type" => "fruit_cultivar"
                }, {
                  "id" => "1",
                  "type" => "fruit_cultivar"
                }]
              }
            },
          },
          "included" => [{
            "attributes" => {
                "fruit_id" => 1, "name" => "Fuji"
              },
              "id" => "2",
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
             }, {
              "attributes" => {
                "fruit_id" => 1, "name" => "Gala"
              },
              "id" => "1",
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            }],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end
  end

  get '/api/v1/fruits?include=:relationship' do

    parameter :relationship, "Model (must be related to record)"

    context '200' do
    
      before do
        @apple = FactoryBot.create(:fruit, name: 'Apple')
        @gala = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Gala")
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Fuji")

        @orange = FactoryBot.create(:fruit, name: 'Orange')
        @blood_orange = FactoryBot.create(:fruit_cultivar, fruit: @orange, name: "Blood Orange")
      end

      let(:relationship) { 'fruit_cultivars' }

      example_request 'Get Fruits with Fruit Cultivars (200)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
            "attributes" => {
              "name" => "Apple"
            },
            "id" => @apple.id.to_s,
            "relationships" => {
              "fruit_cultivars" => {
                "data" => [{
                  "id" => @fuji.id.to_s,
                  "type" => "fruit_cultivar",
                }, {
                  "id" => @gala.id.to_s,
                  "type" => "fruit_cultivar"
                }]
              }
            },
            "type" => "fruit"
          }, {
            "attributes" => {
              "name" => "Orange"
            },
            "id" => @orange.id.to_s,
            "relationships" => {
              "fruit_cultivars" => {
                "data" => [{
                  "id" => @blood_orange.id.to_s,
                  "type" => "fruit_cultivar"
                }]
              }
            },
            "type" => "fruit"
          }],
          "included" => [{
            "attributes" => {
              "fruit_id" => @apple.id,
              "name" => "Fuji"
            },
            "id" => @fuji.id.to_s,
            "type" => "fruit_cultivar",
            "relationships" => { "fruit" => { "meta" => { "included" => false } } }
          }, {
            "attributes" => {
              "fruit_id" => @apple.id, "name" => "Gala"
            },
            "id" => @gala.id.to_s,
            "type" => "fruit_cultivar",
            "relationships" => { "fruit" => { "meta" => { "included" => false } } }
          }, {
            "attributes" => {
              "fruit_id" => @orange.id, "name" => "Blood Orange"
            },
            "id" => @blood_orange.id.to_s,
            "type" => "fruit_cultivar",
            "relationships" => { "fruit" => { "meta" => { "included" => false } } }
          }],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end
  end

  post '/api/v1/fruits' do
    with_options scope: :fruit do
      parameter :name, 'Name of the fruit you want to create', required: true
    end

    context '204' do

      before do

        log_in_as_admin

      end

      let(:raw_post) do
        {
          fruit: {
            name: 'Banana'
          }
        }.to_json
      end

      example_request 'Create a Fruit (204)' do

        expect(status).to eq(204)

        expect(Fruit.all.size).to eq(1)
 
      end
    end

    context '403' do

      before do

        log_in_as_user

      end

      let(:raw_post) do
        {
          fruit: {
            name: 'Banana'
          }
        }.to_json
      end

      example_request 'Create a Fruit (403: Access Denied)' do

        expect(status).to eq(403)

        expected_response = {
          "errors" => [
            {
              "detail" => "You are not authorized to access this page.",
              "status" => "403",
              "title" => "Access Denied"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(Fruit.all.size).to eq(0)
 
      end
    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:raw_post) do
        {
          fruit: {
            not_an_expected_param: 'Garbage'
          }
        }.to_json
      end

      example_request 'Create a Fruit (422: Name Is Missing)' do

        expect(status).to eq(422)

        expect(Fruit.all.size).to eq(0)
 
         expected_response = {
          "errors" => { "name" => ["is too short (minimum is 1 character)"] }
        }
 
        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:raw_post) do
        {
          fruit: {
            name: Array.new(101){ 'a' }.join
          }
        }.to_json
      end

      example_request 'Create a Fruit (422: Name Is Too Long)' do

        expect(status).to eq(422)

        expect(Fruit.all.size).to eq(0)
 
         expected_response = {
          "errors" => { "name" => ["is too long (maximum is 100 characters)"] }
        }
 
        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '422' do

      before do

        log_in_as_admin

        FactoryBot.create(:fruit, name: 'aPPLE')

      end

      let(:raw_post) do
        {
          fruit: {
            name: 'Apple'
          }
        }.to_json
      end

      example_request 'Create a Fruit (422: Name Is Not Unique)' do

        expect(status).to eq(422)

        expected_response = {
          "errors" => { "name" => ["has already been taken"] }
          # Notice the arrows; the colons in the following fail:
          # "data": { "ping": "pong" }
        }
 
        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(Fruit.all.size).to eq(1)

      end
    end

  end

  put '/api/v1/fruits/:id' do

    parameter :id, "Record's ID", required: true
    with_options scope: :fruit do
      parameter :name, 'New name', required: true
    end

    context '204' do

      before do

        log_in_as_admin

      end

      let(:fruit) { FactoryBot.create(:fruit, name: 'Apple') }
      let(:id) { fruit.id }
      let(:raw_post) do
        {
          fruit: {
            name: 'Banana'
          }
        }.to_json
      end

      example_request 'Update a Fruit (204)' do

        expect(status).to eq(204)

        expect(Fruit.all.size).to eq(1)

        expect(Fruit.first['name']).to eq('Banana')

      end

    end

    context '403' do

      let(:fruit) { FactoryBot.create(:fruit, name: 'Apple') }
      let(:id) { fruit.id }
      let(:raw_post) do
        {
          fruit: {
            name: 'Banana'
          }
        }.to_json
      end

      example_request 'Update a Fruit (403: Access Denied)' do

        expect(status).to eq(403)

        expected_response = {
          "errors" => [
            {
              "detail" => "You are not authorized to access this page.",
              "status" => "403",
              "title" => "Access Denied"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(Fruit.all.size).to eq(1)

        expect(Fruit.first['name']).to eq('Apple')

      end

    end

    context '404' do

      before do

        log_in_as_admin

      end

      let(:id) { 999 }

      example_request 'Update a Fruit (404)' do

        expect(status).to eq(404)

          expected_response = {
            "errors" => [
              {
                "status" => "404",
                "title" => "Record not found",
                "detail" => "Couldn't find Fruit with 'id'=999"
              }
            ]
          }
   
          expect(JSON.parse(response_body)).to eq(expected_response)

      end

    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:fruit) { FactoryBot.create(:fruit, name: 'Apple') }
      let(:id) { fruit.id }
      let(:raw_post) do
        {
          fruit: {
            name: ''
          }
        }.to_json
      end

      example_request 'Update a Fruit (422: Name Is Too Short)' do

        expect(status).to eq(422)

        expected_response = {
          "errors" => {"name"=>["is too short (minimum is 1 character)"]}
        }
 
        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(Fruit.all.size).to eq(1)

        expect(Fruit.first['name']).to eq('Apple')

      end

    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:fruit) { FactoryBot.create(:fruit, name: 'Apple') }
      let(:id) { fruit.id }
      let(:raw_post) do
        {
          fruit: {
            not_an_expected_param: 'Garbage'
          }
        }.to_json
      end

      example_request 'Update a Fruit (422: Name Is Missing)' do

        expect(status).to eq(422)

        expected_response = {
          "errors" => {"name"=>["is too short (minimum is 1 character)"]}
        }
 
        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(Fruit.all.size).to eq(1)

        expect(Fruit.first['name']).to eq('Apple')

      end

    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:fruit) { FactoryBot.create(:fruit, name: 'Apple') }
      before { FactoryBot.create(:fruit, name: 'Banana') }
      let(:id) { fruit.id }
      let(:raw_post) do
        {
          fruit: {
            name: 'Banana'
          }
        }.to_json
      end

      example_request 'Update a Fruit (422: Name Is Not Unique)' do

        expect(status).to eq(422)

        expected_response = {
          "errors" => {"name"=>["has already been taken"]}
        }
 
        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(Fruit.all.size).to eq(2)

      end

    end

  end

end
