require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource 'Fruit Cultivar' do

  explanation "Examples of N-to-1 (aka 'many-to-one') database relationships and their resources."

  before(:each) do
    header 'Content-Type', 'application/json'
  end

  delete '/api/v1/fruit_cultivars/:id' do

    parameter :id, "Record's ID", required: true

    context '204' do

      before do

        log_in_as_admin

        @apple = FactoryBot.create(:fruit, name: 'Apple')
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Fuji")

      end

      let(:id) { @fuji.id }

      example_request 'Delete a Fruit Cultivar (204)' do

        expect(status).to eq(204)

        expect(Fruit.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(0)
 
      end
    end

    context '204' do

      before do

        log_in_as_admin

      end

      let(:id) { 999 }

      example_request 'Delete a Fruit Cultivar (204: Records Were Already Deleted)' do
        # Oops we didn't do this correctly before, so we're fixing it now.
        expect(status).to eq(204)
 
      end
    end

    context '403' do

      before do

        @apple = FactoryBot.create(:fruit, name: 'Apple')
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Fuji")

      end

      let(:id) { @fuji.id }

      example_request 'Delete a Fruit Cultivar (403: Access Denied)' do

        expect(status).to eq(403)

        expected_response = {
          "errors" => [
            {
              "detail" => "You are not authorized to access this page.",
              "status" => "403",
              "title" => "Access Denied"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(Fruit.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
 
      end
    end

  end

  get '/api/v1/fruit_cultivars' do

    context '200' do

      before do

        @apple = FactoryBot.create(:fruit, name: 'Apple')

        #
        # Create orange data to ensure it doesn't appear in response.
        #
        @orange = FactoryBot.create(:fruit, name: 'Orange')
        @blood_orange = FactoryBot.create(:fruit_cultivar, fruit: @orange, name: "Blood Orange")

        #
        # The following variables must use @ to be accessible from within the test.
        # The @ symbol makes the variables "instance variables".
        #
        # Defining them with let does not work because the let variable is not
        # present in the API request's URL or body.
        #
        @gala = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Gala")
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Fuji")
        
      end

      example_request "Get all Fruit Cultivars (200)" do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "fruit_id" => @orange.id, "name" => "Blood Orange"
              },
              "id" => @blood_orange.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Gala"
              },
              "id" => @gala.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Fuji"
              },
              "id" => @fuji.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)
 
      end
    end

  end

  get '/api/v1/fruit_cultivars?include=:relationship' do

    parameter :relationship, "Model (must be related to record)"

    let(:relationship) { 'fruit' }

    context '200' do

      before do

        @apple = FactoryBot.create(:fruit, name: 'Apple')

        #
        # Create orange data to ensure it doesn't appear in response.
        #
        @orange = FactoryBot.create(:fruit, name: 'Orange')
        @blood_orange = FactoryBot.create(:fruit_cultivar, fruit: @orange, name: "Blood Orange")

        #
        # The following variables must use @ to be accessible from within the test.
        # The @ symbol makes the variables "instance variables".
        #
        # Defining them with let does not work because the let variable is not
        # present in the API request's URL or body.
        #
        @gala = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Gala")
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Fuji")
        
      end

      example_request "Get all Fruit Cultivars with Fruit (200)" do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "fruit_id" => @orange.id, "name" => "Blood Orange"
              },
              "id" => @blood_orange.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "data" => { "id" => @orange.id.to_s, "type" => "fruit" } } },
            },
            {
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Gala"
              },
              "id" => @gala.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "data" => { "id" => @apple.id.to_s, "type" => "fruit" } } },
            },
            {
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Fuji"
              },
              "id" => @fuji.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "data" => { "id" => @apple.id.to_s, "type" => "fruit" } } },
            }
          ],
          "included" => [{
            "attributes" => {
              "name" => "Orange"
            },
            "id" => @orange.id.to_s,
            "relationships" => {
              "fruit_cultivars" => {
                "meta" => {
                  "included" => false
                }
              }
            },
            "type" => "fruit"
          }, {
            "attributes" => {
              "name" => "Apple"
            },
            "id" => @apple.id.to_s,
            "relationships" => {
              "fruit_cultivars" => {
                "meta" => {
                  "included" => false
                }
              }
            },
            "type" => "fruit"
          }],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)
 
      end
    end

  end

  get '/api/v1/fruit_cultivars?sort=:sort' do

    parameter :sort, 'Sort by name (asc or desc)', required: false

    before do

      @apple = FactoryBot.create(:fruit, name: 'Apple')

      #
      # Create orange data to ensure it doesn't appear in response.
      #
      @orange = FactoryBot.create(:fruit, name: 'Orange')
      @blood_orange = FactoryBot.create(:fruit_cultivar, fruit: @orange, name: "Blood Orange")

      #
      # The following variables must use @ to be accessible from within the test.
      # The @ symbol makes the variables "instance variables".
      #
      # Defining them with let does not work because the let variable is not
      # present in the API request's URL or body.
      #
      @gala = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Gala")
      @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Fuji")

    end

    let(:fruit_id) { @apple.id }

    context '200' do

      let(:sort) { 'asc' }

      example_request "Get all Fruit Cultivars (200: Sort By Name, Ascending)" do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "fruit_id" => @orange.id, "name" => "Blood Orange"
              },
              "id" => @blood_orange.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Fuji"
              },
              "id" => @fuji.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Gala"
              },
              "id" => @gala.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '200' do

      let(:sort) { 'desc' }

      example_request "Get all Fruit Cultivars (200: Sort By Name, Descending)" do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Gala"
              },
              "id" => @gala.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Fuji"
              },
              "id" => @fuji.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "fruit_id" => @orange.id, "name" => "Blood Orange"
              },
              "id" => @blood_orange.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '200' do

      let(:sort) { 'BAD_VALUE' }

      example_request "Get all Fruit Cultivars (200: Sort By Name, Bad Value)" do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "fruit_id" => @orange.id, "name" => "Blood Orange"
              },
              "id" => @blood_orange.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Fuji"
              },
              "id" => @fuji.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Gala"
              },
              "id" => @gala.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end
  end

  get '/api/v1/fruit_cultivars/:id' do

    parameter :id, "Record's ID", required: true

    context '200' do

      before do
        @apple = FactoryBot.create(:fruit, name: 'Apple')
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: 'Fuji')
      end

      let(:id) { @fuji.id }

      example_request 'Get a Fruit Cultivar (200)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => {
            "attributes" => {
              "name" => "Fuji",
              "fruit_id" => @apple.id
            },
            "id" => @fuji.id.to_s,
            "type" => "fruit_cultivar",
            "relationships" => { "fruit" => { "meta" => { "included" => false } } }
          },
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '404' do

      let(:id) { 999 }

      example_request 'Get a Fruit Cultivar (404)' do

        expect(status).to eq(404)

        expected_response = {
          "errors" => [
            {
              "detail" => "Couldn't find FruitCultivar with 'id'=999",
              "status" => "404",
              "title" => "Record not found"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end

    end
  end

  get '/api/v1/fruit_cultivars/:id?include=:relationship' do

    parameter :id, "Record's ID", required: true
    parameter :relationship, "Model (must be related to record)"

    context '200' do

      before do
        @apple = FactoryBot.create(:fruit, id: 1, name: 'Apple')
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Fuji")
      end

      let(:id) { @fuji.id }
      let(:relationship) { 'fruit' }

      example_request 'Get a Fruit Cultivar with its Fruit (200)' do

        expect(status).to eq(200)

        expected_response = {
          "data" => {
            "attributes" => {
              "name" => "Fuji",
              "fruit_id" => @apple.id
            },
            "id" => "1",
            "type" => "fruit_cultivar",
            "relationships" => {
              "fruit" => {
                "data" => {
                  "id" => @apple.id.to_s,
                  "type" => "fruit"
                }
              }
            },
          },
          "included" => [{
            "attributes" => {
                "name" => "Apple"
              },
              "id" => @apple.id.to_s,
              "type" => "fruit",
              "relationships" => { "fruit_cultivars" => { "meta" => { "included" => false } } }
             }],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end
  end

  get '/api/v1/fruits/:fruit_id/fruit_cultivars' do

    context '200' do

      before do

        @apple = FactoryBot.create(:fruit, name: 'Apple')

        #
        # Create orange data to ensure it doesn't appear in response.
        #
        orange = FactoryBot.create(:fruit, name: 'Orange')
        FactoryBot.create(:fruit_cultivar, fruit: orange, name: "Blood Orange")

        #
        # The following variables must use @ to be accessible from within the test.
        # The @ symbol makes the variables "instance variables".
        #
        # Defining them with let does not work because the let variable is not
        # present in the API request's URL or body.
        #
        @gala = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Gala")
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Fuji")
        
      end

      let(:fruit_id) { @apple.id }

      example_request "Get a Fruit's Cultivars (200)" do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Fuji"
              },
              "id" => @fuji.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Gala"
              },
              "id" => @gala.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)
 
      end
    end

    context '404' do

      let(:fruit_id) { 999 }

      example_request "Get a Fruit's Cultivars (404: Fruit Not Found)" do

        expect(status).to eq(404)

        expected_response = { "errors" => [ {"detail" => "Couldn't find Fruit with 'id'=999", "status" => "404", "title" => "Record not found" } ] }

        expect(JSON.parse(response_body)).to eq(expected_response)
 
      end
    end

  end

  #
  # For more information on how to write specs for documentation, see:
  #
  #     https://github.com/zipmark/rspec_api_documentation
  #
  get '/api/v1/fruits/:fruit_id/fruit_cultivars?sort=:sort' do

    parameter :sort, 'Sort by name (asc or desc)', required: false

    before do

      @apple = FactoryBot.create(:fruit, name: 'Apple')

      #
      # Create orange data to ensure it doesn't appear in response.
      #
      orange = FactoryBot.create(:fruit, name: 'Orange')
      FactoryBot.create(:fruit_cultivar, fruit: orange, name: "Blood Orange")

      #
      # The following variables must use @ to be accessible from within the test.
      # The @ symbol makes the variables "instance variables".
      #
      # Defining them with let does not work because the let variable is not
      # present in the API request's URL or body.
      #
      @gala = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Gala")
      @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: "Fuji")
      
    end

    let(:fruit_id) { @apple.id }

    context '200' do

      let(:sort) { 'asc' }

      example_request "Get a Fruit's Cultivars (200: Sort By Name, Ascending)" do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Fuji"
              },
              "id" => @fuji.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Gala"
              },
              "id" => @gala.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '200' do

      let(:sort) { 'desc' }

      example_request "Get a Fruit's Cultivars (200: Sort By Name, Descending)" do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Fuji"
              },
              "id" => @fuji.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Gala"
              },
              "id" => @gala.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '200' do

      let(:sort) { 'BAD_VALUE' }

      example_request "Get a Fruit's Cultivars (200: Sort By Name, Bad Value)" do

        expect(status).to eq(200)

        expected_response = {
          "data" => [{
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Fuji"
              },
              "id" => @fuji.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            },
            {
              "attributes" => {
                "fruit_id" => @apple.id, "name" => "Gala"
              },
              "id" => @gala.id.to_s,
              "type" => "fruit_cultivar",
              "relationships" => { "fruit" => { "meta" => { "included" => false } } }
            }
          ],
          "jsonapi" => {
            "version" => "1.0"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end
  end

  post '/api/v1/fruits/:fruit_id/fruit_cultivars' do
    with_options scope: :fruit_cultivar do
      parameter :name, 'Name of the fruit cultivar you want to create', required: true
    end

    context '204' do

      let(:fruit) { FactoryBot.create(:fruit, name: 'Apple') }

      let(:fruit_id) { fruit.id }

      before do

        log_in_as_admin

        #
        # Ensure no collision on same name but different fruit.
        #
        orange = FactoryBot.create(:fruit, name: 'Orange')
        FactoryBot.create(:fruit_cultivar, fruit: orange, name: 'Fuji')

      end

      let(:raw_post) do
        {
          fruit_cultivar: {
            name: 'Fuji'
          }
        }.to_json
      end

      example_request 'Create a Fruit Cultivar (204)' do

        expect(status).to eq(204)

        expect(FruitCultivar.all.size).to eq(2)
 
      end
    end

    context '403' do

      let(:fruit) { FactoryBot.create(:fruit, name: 'Apple') }

      let(:fruit_id) { fruit.id }

      before do

        #
        # Ensure no collision on same name but different fruit.
        #
        orange = FactoryBot.create(:fruit, name: 'Orange')
        FactoryBot.create(:fruit_cultivar, fruit: orange, name: 'Fuji')

      end

      let(:raw_post) do
        {
          fruit_cultivar: {
            name: 'Fuji'
          }
        }.to_json
      end

      example_request 'Create a Fruit Cultivar (403: Access Denied)' do

        expect(status).to eq(403)

        expected_response = {
          "errors" => [
            {
              "detail" => "You are not authorized to access this page.",
              "status" => "403",
              "title" => "Access Denied"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitCultivar.all.size).to eq(1)
 
      end
    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:fruit_id) { 999 }

      let(:raw_post) do
        {
          fruit_cultivar: {
            name: 'Fuji'
          }
        }.to_json
      end

      example_request 'Create a Fruit Cultivar (422: Fruit Does Not Exist)' do

        expect(status).to eq(422)

        expect(FruitCultivar.all.size).to eq(0)
 
          expected_response = {
          "errors" => { "fruit" => ["must exist"] }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:fruit) { FactoryBot.create(:fruit, name: 'Apple') }

      let(:fruit_id) { fruit.id }

      let(:raw_post) do
        {
          fruit_cultivar: {
            not_an_expected_param: 'Garbage'
          }
        }.to_json
      end

      example_request 'Create a Fruit Cultivar (422: Name Is Missing)' do

        expect(status).to eq(422)

        expect(FruitCultivar.all.size).to eq(0)

         expected_response = {
          "errors" => { "name" => ["is too short (minimum is 1 character)"] }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:fruit) { FactoryBot.create(:fruit, name: 'Apple') }

      let(:fruit_id) { fruit.id }

      let(:raw_post) do
        {
          fruit_cultivar: {
            name: Array.new(101){ 'a' }.join
          }
        }.to_json
      end

      example_request 'Create a Fruit Cultivar (422: Name Is Too Long)' do

        expect(status).to eq(422)

        expect(FruitCultivar.all.size).to eq(0)

         expected_response = {
          "errors" => { "name" => ["is too long (maximum is 100 characters)"] }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '422' do

      before do

        log_in_as_admin

      end

      let(:fruit) { FactoryBot.create(:fruit, name: 'Apple') }

      let(:fruit_id) { fruit.id }

      before do
        FactoryBot.create(:fruit_cultivar, fruit: fruit, name: 'Fuji')
      end

      let(:raw_post) do
        {
          fruit_cultivar: {
            name: 'fUjI' # Test case-insensitive while we're at it
          }
        }.to_json
      end

      example_request 'Create a Fruit Cultivar (422: Name Is Not Unique)' do

        expect(status).to eq(422)

        expected_response = {
          "errors" => { "name" => ["has already been taken"] }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitCultivar.all.size).to eq(1)

      end
    end

  end

  put '/api/v1/fruit_cultivars/:id' do

    parameter :id, "Record's ID", required: true
    with_options scope: :fruit_cultivar do
      parameter :name, 'New name', required: true
    end

    context '204' do

      before do

        log_in_as_admin

        @apple = FactoryBot.create(:fruit, name: 'Apple')
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: 'Fuji')

        #
        # Test that Apple's Fuji cultivar can be renamed to Gala even though
        # Gala Oranges exist.
        #
        orange = FactoryBot.create(:fruit, name: 'Orange')
        FactoryBot.create(:fruit_cultivar, fruit: orange, name: 'Gala')
      end

      let(:id) { @fuji.id }
      let(:raw_post) do
        {
          fruit_cultivar: {
            name: 'Gala'
          }
        }.to_json
      end

      example_request 'Update a Fruit Cultivar (204)' do

        expect(status).to eq(204)

        expect(FruitCultivar.all.size).to eq(2)

        expect(FruitCultivar.first['name']).to eq('Gala')

      end

    end

    context '403' do

      before do

        @apple = FactoryBot.create(:fruit, name: 'Apple')
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: 'Fuji')

        #
        # Test that Apple's Fuji cultivar can be renamed to Gala even though
        # Gala Oranges exist.
        #
        orange = FactoryBot.create(:fruit, name: 'Orange')
        FactoryBot.create(:fruit_cultivar, fruit: orange, name: 'Gala')
      end

      let(:id) { @fuji.id }
      let(:raw_post) do
        {
          fruit_cultivar: {
            name: 'Gala'
          }
        }.to_json
      end

      example_request 'Update a Fruit Cultivar (403: Access Denied)' do

        expect(status).to eq(403)

        expected_response = {
          "errors" => [
            {
              "detail" => "You are not authorized to access this page.",
              "status" => "403",
              "title" => "Access Denied"
            }
          ]
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitCultivar.all.size).to eq(2)

        expect(FruitCultivar.first['name']).to eq('Fuji')

      end

    end

    context '404' do

      before do

        user = FactoryBot.create(:user)
        auth_header_for_user(user)

      end

      let(:id) { 999 }

      example_request 'Update a Fruit Cultivar (404)' do

        expect(status).to eq(404)

          expected_response = {
            "errors" => [
              {
                "status" => "404",
                "title" => "Record not found",
                "detail" => "Couldn't find FruitCultivar with 'id'=999"
              }
            ]
          }

          expect(JSON.parse(response_body)).to eq(expected_response)

      end

    end

    context '422' do

      before do

        log_in_as_admin

        @apple = FactoryBot.create(:fruit, name: 'Apple')
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: 'Fuji')

      end

      let(:id) { @fuji.id }
      let(:raw_post) do
        {
          fruit_cultivar: {
            name: ''
          }
        }.to_json
      end

      example_request 'Update a Fruit Cultivar (422: Name Is Too Short)' do

        expect(status).to eq(422)

        expected_response = {
          "errors" => {"name"=>["is too short (minimum is 1 character)"]}
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitCultivar.all.size).to eq(1)

        expect(FruitCultivar.first['name']).to eq('Fuji')

      end

    end

    context '422' do

      before do

        log_in_as_admin

        @apple = FactoryBot.create(:fruit, name: 'Apple')
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: 'Fuji')
      end

      let(:id) { @fuji.id }
      let(:raw_post) do
        {
          fruit_cultivar: {
            not_an_expected_param: 'Garbage'
          }
        }.to_json
      end

      example_request 'Update a Fruit Cultivar (422: Name Is Missing)' do

        expect(status).to eq(422)

        expected_response = {
          "errors" => {
            "name" => ["is too short (minimum is 1 character)"]
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitCultivar.all.size).to eq(1)

        expect(FruitCultivar.first['name']).to eq('Fuji')

      end

    end

    context '422' do

      before do

        log_in_as_admin

        @apple = FactoryBot.create(:fruit, name: 'Apple')
        @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: 'Fuji')
        @gala = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: 'Gala')
      end

      let(:id) { @fuji.id }
      let(:raw_post) do
        {
          fruit_cultivar: {
            name: 'Gala'
          }
        }.to_json
      end

      example_request 'Update a Fruit Cultivar (422: Name Is Not Unique)' do

        expect(status).to eq(422)

        expected_response = {
          "errors" => {"name"=>["has already been taken"]}
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

        expect(FruitCultivar.all.size).to eq(2)

      end

    end

  end

end
