require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource 'User Authentication' do

  explanation "User authentication endpoints."

  before(:each) do
    header 'Content-Type', 'application/json'
  end

  post '/api/v1/authenticate' do

    parameter :email, "User's email address", required: true
    parameter :password, "User's password", required: true

    context '200' do

      before do
        @email = 'test@localhost.com'
        @password = 'MyP@ssword1'
        FactoryBot.create(:user, email: @email, password: @password)
      end

      let(:raw_post) do
        {
          email: @email,
          password: @password
        }.to_json
      end

      example_request 'Authenticate User (200)' do

        expect(status).to eq(200)

        response = JSON.parse(response_body)
        expect(response).to have_key('auth_token')

        # We can't assert the actual response value, because the auth_token will always be different.

      end
    end

    context '401' do

      before do
        @email = 'test@localhost.com'
        @password = 'MyP@ssword1'
        FactoryBot.create(:user, email: @email, password: @password)
      end

      let(:raw_post) do
        {
          email: @email,
          password: "Not_The_Password"
        }.to_json
      end

      example_request 'Authenticate User (401: Bad Password)' do

        expect(status).to eq(401)

        expected_response = {
          "error" => {
            "user_authentication" => "invalid credentials"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

    context '401' do

      let(:raw_post) do
        {
          email: "does@not.exist",
          password: "Not_The_Password"
        }.to_json
      end

      example_request "Authenticate User (401: Email Doesn't Exist)" do

        expect(status).to eq(401)

        expected_response = {
          "error" => {
            "user_authentication" => "invalid credentials"
          }
        }

        expect(JSON.parse(response_body)).to eq(expected_response)

      end
    end

  end
end
