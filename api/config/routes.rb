Rails.application.routes.draw do

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      post 'authenticate', to: 'authentication#authenticate'
      get 'ping', to: 'ping#show' # GET localhost:PORT/api/v1/ping
      resources :fruits, only: [:create, :destroy, :index, :show, :update] do # Notice 'resource' fails to find both routes but 'resources' works. You can check this on command line with 'rails routes'.
        #
        # We are nesting the fruit_cultivars resource to create the desired route format.
        #
        # The following creates "POST /api/v1/fruits/:fruit_id/fruit_cultivars".
        #
        # If we wanted it to be "POST /api/v1/fruits/:fruit_id/cultivars" then we
        # would have to explicitly define the route, like we did with ping. I prefer
        # using the full name, fruit_cultivar, even though it may seem redundant in
        # the URL.
        #
        # Remember to use "$ rails routes" to see what routes are being generated.
        #
        # Also, to reduce the number of endpoints, it would be better to add an
        # "?includes=fruit_cultivar" to the GET fruits endpoints. We can do this
        # easily with the jsonapi-rails gem, but not now, because it will overly
        # complicate this diff.
        #
        resources :fruit_cultivars, only: [:create, :index]
      end

      resources :fruit_cultivars, only: [:destroy, :index, :show, :update]

      resources :fruit_stands, only: [:create, :destroy, :index, :show, :update] do
        #
        # On member vs collection:
        # 
        #   A member route will require an ID, because it acts on a member (a single object).
        #   A collection route doesn't because it acts on a collection of objects.
        #
        #   https://stackoverflow.com/questions/3028653/difference-between-collection-route-and-member-route-in-ruby-on-rails
        # 
        member do
          #
          # Don't forget to use the following to see what routes are generated: $ rails routes
          #
          post :fruits, to: 'fruit_stands#add_fruit'
          delete 'fruits/:fruit_id', to: 'fruit_stands#remove_fruit'
        end
      end

      resources :users, only: [:create] do
        member do
          put :confirm_email, to: 'users#confirm_email'
        end
      end

    end
  end

end
