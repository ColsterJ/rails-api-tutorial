# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_20_170800) do

  create_table "fruit_cultivars", force: :cascade do |t|
    t.string "name"
    t.integer "fruit_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["fruit_id", "name"], name: "index_fruit_cultivars_on_fruit_id_and_name", unique: true
  end

  create_table "fruit_stands", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_fruit_stands_on_name", unique: true
  end

  create_table "fruit_stands_fruits", id: false, force: :cascade do |t|
    t.integer "fruit_stand_id"
    t.integer "fruit_id"
    t.index ["fruit_stand_id", "fruit_id"], name: "index_fruit_stands_fruits_on_fruit_stand_id_and_fruit_id", unique: true
  end

  create_table "fruits", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_fruits_on_name", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email_confirmation_code", limit: 8
    t.string "role"
    t.index ["email"], name: "index_users_on_email_index", unique: true
  end

  add_foreign_key "fruit_cultivars", "fruits"
  add_foreign_key "fruit_stands_fruits", "fruit_stands"
  add_foreign_key "fruit_stands_fruits", "fruits"
end
