class CreateFruitStandsFruits < ActiveRecord::Migration[6.0]
  # For more information, see http://railscasts.com/episodes/47-two-many-to-many
  def change
    create_table :fruit_stands_fruits, :id => false do |t|
      t.column :fruit_stand_id, :integer
      t.column :fruit_id, :integer
    end

    #
    # The fruit_stand_id should reference the fruit_stand record.
    # Because of naming conventions, this knows to link
    # fruit_stands_fruits.fruit_stand_id to fruit_stands.id.
    #
    add_foreign_key :fruit_stands_fruits, :fruit_stands

    #
    # The fruit_id should reference the fruit record.
    # Because of naming conventions, this knows to link
    # fruit_stands_fruits.fruit_id to fruits.id.
    #
    add_foreign_key :fruit_stands_fruits, :fruits

    #
    # Dual-key unique constraint on fruit and fruit_stand so we don't have
    # duplicates.
    #    
    add_index :fruit_stands_fruits, [:fruit_stand_id, :fruit_id], :unique => true

  end
end
