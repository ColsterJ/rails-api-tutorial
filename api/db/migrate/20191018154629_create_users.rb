class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :password_digest

      t.timestamps
    end

    #
    # OOPS - this was a mistake. This causes the table to only allow one users
    # record in SQLite and I'm unable to drop the index. Things are just corrupt.
    # So, we're going to edit an old migration file, which you normally shouldn't
    # do.
    #
    # add_index :users, ['lower(email)'], :name => "index_users_on_lower_email_index", :unique => true
  end
end
