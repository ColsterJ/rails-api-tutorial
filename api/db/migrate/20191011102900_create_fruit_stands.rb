class CreateFruitStands < ActiveRecord::Migration[6.0]
  def change
    create_table :fruit_stands do |t|
      t.string :name

      t.timestamps
    end

    add_index :fruit_stands, :name, unique: true

  end
end
