class CreateFruits < ActiveRecord::Migration[6.0]
  def change
    create_table :fruits do |t|
      t.string :name

      t.timestamps
    end

    add_index :fruits, :name, unique: true
  end
end
