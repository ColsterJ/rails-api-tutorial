class UpdateUsersEmailConfirmationCode < ActiveRecord::Migration[6.0]
  def change
    #
    # We want to add a column to the users table, but we've already released
    # the users table migration. So, we have to create a new migration to add
    # the column.
    #
    change_table :users do |t|
      #
      # We add a comment directly to the database column so we can understand
      # its purpose when just looking at the database's schema.
      #
      # Note that comments are only added to certain databases, probably MySQL,
      # but it does not seem like you can add comments to SQLite databases so
      # if you run migrations on SQLite, this likely won't appear in schema.rb
      # either.
      #
      t.string :email_confirmation_code, :limit => 8, :comment => 'User can only be authenticated when this is empty.'
    end
  end
end
